
const getPoilcyByPolicyId = (policyId) => {
    return fetch(`http://127.0.0.1:8000/Policies/?policyId=${policyId}`).then(
        response => {
            return response.json()
        }
    )
}

const getPolicies = () => {
    return fetch(`http://127.0.0.1:8000/Policies/`).then(
        response => {
            return response.json()
        }
    )
}

const getPoilcyByCustomerId = (customerId) => {
    return fetch(`http://127.0.0.1:8000/Policies/?customerId=${customerId}`).then(
        response => {
            return response.json()
        }
    )
}

export default {
    getPoilcyByPolicyId,
    getPoilcyByCustomerId,
    getPolicies
}