import './App.css';
import PolicyDetails from './Components/PolicyView/PolicyView';

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <h1>ABC Insurance Ltd.</h1>
       <PolicyDetails></PolicyDetails>
      </header>
    </div>
  );
}

export default App;
