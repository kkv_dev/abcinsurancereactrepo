import React, { useRef, useState } from "react";
import policyRequests from '../../Requests/policyRequests'
import './PolicySeacrh.css'

const PolicySearch = props => {

    const[searchBoxLabelText, setSearchBoxLabelText] = useState('Enter Policy Id')
    const[searhBy, setSearchBy] = useState('policyId')

    const dropDownSelectHandler = (e) =>{
        if(e.target.value === 'policyId'){
            setSearchBoxLabelText('Enter Policy Id')
            setSearchBy('policyId')
        }else {
            setSearchBoxLabelText('Enter Customer Id')
            setSearchBy('customerId')
        }
    }
    
    const searchValueRef = useRef()

    const searchHandler = (e) => {
        e.preventDefault()
        let id = searchValueRef.current.value
        if(searhBy === 'policyId'){
            policyRequests.getPoilcyByPolicyId(id).then(
                policy => props.searchResult(policy)
            )
        }else{
            policyRequests.getPoilcyByCustomerId(id).then(
                policy => props.searchResult(policy)
            )
        }
    }

    return (
        <div className="searchBox">
            <label> <span>Search By : </span>
            <select id="searchType" className="textbox" onChange={dropDownSelectHandler}>
                <option value="policyId"> Policy ID </option>
                <option value="customerId"> Customer Id </option>
            </select>
            </label>
            <label> <span>{searchBoxLabelText} : </span> <input className="textbox" type="search" ref={searchValueRef}></input> </label>
            <button type="submit" className="btn" onClick={searchHandler}>Search</button>
        </div>
    )

}

export default PolicySearch