import React, { useState, useEffect } from "react";
import policyRequests from "../../Requests/policyRequests";
import PolicySearch from "../PolicySearch/PolicySearch";
import "./PolicyView.css";

const PolicyDetails = (props) => {
  const [policies, setPolicies] = useState({ data: [], hasData: false });

  useEffect(() => {
    policyRequests.getPolicies().then((policyRecords) => {
      setPolicies({ data: policyRecords, hasData: true });
    });
  }, []);

  const searchResultHandler = (policyRecord) => {
    setPolicies({ data: policyRecord, hasData: true });
  };

  return (
    <div className="container">
      <PolicySearch searchResult={searchResultHandler}></PolicySearch>
      <table className="table">
        <thead>
          <tr>
            <th>Policy ID</th>
            <th>Customer ID</th>
            <th>Date Of Purchase</th>
            <th>Vehicle Segment</th>
            <th>Vehicle Fuel Type</th>
            <th>Premium Amount</th>
            <th>Personal Injury Protection</th>
            <th>Property Damage Liability</th>
            <th>Bodily Injury Liability</th>
            <th>Collision</th>
            <th>Comprehensive</th>
            <th>Customer Region</th>
          </tr>
        </thead>
        <tbody>
          {policies.hasData &&
            policies.data.map((policy) => {
              return (
                <tr>
                  <td>{policy.POLICY_ID}</td>
                  <td>{policy.CUSTOMER_ID}</td>
                  <td>{policy.DATE_OF_PURCHASE}</td>
                  <td>{policy.VEHICLE_SEGMENT}</td>
                  <td>{policy.FUEL}</td>
                  <td>{policy.PREMIUM}</td>
                  <td>{policy.PERSONAL_INJURY_LIABILITY === 0 ? 'No': 'Yes'}</td>
                  <td>{policy.PROPERTY_DAMAGE_LIABILITY === 0 ? 'No': 'Yes'}</td>
                  <td>{policy.BODILY_INJURY_LIABILITY === 0 ? 'No': 'Yes'}</td>
                  <td>{policy.COLLISION === 0 ? 'No': 'Yes'}</td>
                  <td>{policy.COMPREHENSIVE === 0 ? 'No': 'Yes'}</td>
                  <td>{policy.CUSTOMER_REGION}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default PolicyDetails;
